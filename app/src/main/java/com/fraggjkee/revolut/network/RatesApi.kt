package com.fraggjkee.revolut.network

import com.squareup.moshi.JsonClass
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

    @GET("/latest")
    fun getRates(@Query("base") base: String): Single<GetRatesResponse>
}

@JsonClass(generateAdapter = true)
data class GetRatesResponse(
    val base: String,
    val date: String,
    val rates: Map<String, Double>
)