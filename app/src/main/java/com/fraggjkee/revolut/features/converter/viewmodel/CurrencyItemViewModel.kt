package com.fraggjkee.revolut.features.converter.viewmodel

import androidx.databinding.Observable
import com.fraggjkee.revolut.domain.ConversionRate
import com.fraggjkee.revolut.features.base.Clearable
import com.fraggjkee.revolut.features.converter.BaseAmountStream
import com.fraggjkee.revolut.features.converter.currency.CurrencyConverter
import com.fraggjkee.revolut.features.converter.currency.CurrencyFormatter
import com.fraggjkee.revolut.features.converter.currency.CurrencyIconProvider
import com.fraggjkee.revolut.features.converter.parseDouble
import com.fraggjkee.revolut.features.converter.viewstate.CurrencyItemViewState
import com.fraggjkee.revolut.features.converter.viewstate.update
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

val ConversionRate.isBaseCurrency: Boolean
    get() = this.base == this.target

class CurrencyItemViewModel @Inject constructor(
    private val iconProvider: CurrencyIconProvider,
    private val baseAmountStream: BaseAmountStream,
    private val converter: CurrencyConverter,
    private val formatter: CurrencyFormatter
) : Clearable {

    val viewState = CurrencyItemViewState()

    lateinit var conversionRate: ConversionRate

    private val uiEvents = PublishSubject.create<UiEvent>()
    private val disposables = CompositeDisposable()

    private val amountChangedCallback = object : Observable.OnPropertyChangedCallback() {

        override fun onPropertyChanged(sender: Observable, property: Int) {
            if (conversionRate.isBaseCurrency.not()) return

            val oldAmount = baseAmountStream.value()
            val newAmount = viewState.amount.get()?.parseDouble() ?: 0.0
            if (newAmount != oldAmount) {
                baseAmountStream.post(newAmount)
            }
        }
    }

    init {
        viewState.amount.addOnPropertyChangedCallback(amountChangedCallback)
        subscribeOnViewStateEvents()
    }

    private fun subscribeOnAmountStream() {
        disposables += baseAmountStream.stream()
            .debounce(300, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleBaseAmountChange(it) }
    }

    private fun handleBaseAmountChange(amount: Double) {
        if (conversionRate.isBaseCurrency) return
        val targetAmount = converter.convert(amount, conversionRate)
        viewState.amount.set(formatter.format(targetAmount))
    }

    private fun subscribeOnViewStateEvents() {
        disposables += viewState.events()
            .ofType<CurrencyItemViewState.Clicks.OnCellClick>()
            .subscribe { handleCellClick() }
    }

    private fun handleCellClick() {
        uiEvents.onNext(
            UiEvent.OnCellClick(
                this
            )
        )
    }

    fun setup(conversionRate: ConversionRate) {
        this.conversionRate = conversionRate

        val amount = converter.convert(baseAmountStream.value(), conversionRate)
        viewState.update(
            drawable = iconProvider.provide(conversionRate.target),
            currency = conversionRate.target,
            value = formatter.format(amount),
            editable = conversionRate.isBaseCurrency
        )

        subscribeOnAmountStream()
    }

    fun events(): io.reactivex.Observable<UiEvent> {
        return uiEvents.hide()
    }

    override fun onCleared() {
        viewState.amount.removeOnPropertyChangedCallback(amountChangedCallback)
        disposables.dispose()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CurrencyItemViewModel
        return (conversionRate == other.conversionRate)
    }

    override fun hashCode(): Int = conversionRate.hashCode()

    sealed class UiEvent {
        class OnCellClick(val sender: CurrencyItemViewModel) : UiEvent()
    }
}