package com.fraggjkee.revolut.features.converter.currency

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

private const val FRACTION_DIGITS_MAX = 2
private const val FRACTION_DIGITS_MIN = 0

class CurrencyFormatter @Inject constructor() {

    fun format(
        amount: Double,
        locale: Locale = Locale.getDefault()
    ): String {
        val format = (NumberFormat.getNumberInstance(locale) as DecimalFormat)
            .apply {
                minimumFractionDigits = FRACTION_DIGITS_MIN
                maximumFractionDigits = FRACTION_DIGITS_MAX
            }
        return format.format(amount)
    }
}