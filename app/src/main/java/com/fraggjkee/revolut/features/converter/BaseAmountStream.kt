package com.fraggjkee.revolut.features.converter

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BaseAmountStream @Inject constructor() {

    private val subject = BehaviorSubject.create<Double>()

    fun post(double: Double) = subject.onNext(double)

    fun value(): Double = subject.value ?: throw IllegalStateException("Stream is not initialized")

    fun stream(): Observable<Double> = subject.hide()
}