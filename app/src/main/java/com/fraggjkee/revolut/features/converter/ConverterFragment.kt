package com.fraggjkee.revolut.features.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.fraggjkee.revolut.databinding.FragmentConverterBinding
import com.fraggjkee.revolut.features.base.BaseFragment
import com.fraggjkee.revolut.features.converter.viewmodel.ConverterViewModel
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables

// TODO
// User experience is still far from ideal but I think that this demo project state is good
// enough to demonstrate my current skills. Existing UI glitches can be resolved and some
// things can be improved but most likely all of that will not change the overall architecture.
// For instance, I'd improve input experience for the Amount EditText (InputFilters, etc.). Formatting
// and Conversion can also be improved, currently it is fairly primitive.

// TODO
// Investigate if we can Flowable (backpressure) instead of Observable for ObserveRatesUseCase

// TODO error handling should be extended for a real production-level app:
// - intercept & wrap errors at Retrofit / OkHttp level
// - retry logic
// - connectivity state monitoring
// - placeholder UI + retry button
// - etc.

class ConverterFragment : BaseFragment() {

    private var disposable = Disposables.empty()

    private val viewModel: ConverterViewModel by lazy {
        ViewModelProviders
            .of(this, viewModelFactory)
            .get(ConverterViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentConverterBinding.inflate(inflater, container, false)
            .also { it.viewState = viewModel.viewState }
            .root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disposable = viewModel.events()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleViewModelEvent(it) }
    }

    private fun handleViewModelEvent(event: ConverterViewModel.Event) {
        when (event) {
            is ConverterViewModel.Event.OnError -> showSnackbar(event.message)
        }
    }

    private fun showSnackbar(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(viewModel)
    }
}
