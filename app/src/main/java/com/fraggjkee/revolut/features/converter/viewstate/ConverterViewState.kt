package com.fraggjkee.revolut.features.converter.viewstate

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.fraggjkee.revolut.features.base.RecyclerViewItem
import javax.inject.Inject

class ConverterViewState @Inject constructor() : BaseObservable() {

    val items = ObservableField<List<RecyclerViewItem>>()
    val isLoading = ObservableBoolean(false)

    val isProgressVisible: Boolean
        @Bindable("items", "isLoading")
        get() = items.get().orEmpty().isEmpty() && isLoading.get()
}