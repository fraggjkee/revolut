package com.fraggjkee.revolut.features.converter.viewstate

import android.graphics.drawable.Drawable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.fraggjkee.revolut.domain.Currency
import io.reactivex.subjects.PublishSubject

class CurrencyItemViewState {

    val icon = ObservableField<Drawable>()
    val currencyTitle = ObservableField<String>()
    val currencySubtitle = ObservableField<String>()
    val amount = ObservableField<String>()
    val isEditable = ObservableBoolean()

    private val eventsSubject = PublishSubject.create<Clicks>()

    fun onClick() = eventsSubject.onNext(Clicks.OnCellClick)

    fun events(): io.reactivex.Observable<Clicks> = eventsSubject.hide()

    sealed class Clicks {
        object OnCellClick : Clicks()
    }
}

fun CurrencyItemViewState.update(
    drawable: Drawable?,
    currency: Currency,
    value: String,
    editable: Boolean = false
) {
    icon.set(drawable)
    currencyTitle.set(currency.code)
    currencySubtitle.set(currency.displayName)
    amount.set(value)
    isEditable.set(editable)
}