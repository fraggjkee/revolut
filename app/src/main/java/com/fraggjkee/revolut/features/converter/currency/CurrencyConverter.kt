package com.fraggjkee.revolut.features.converter.currency

import com.fraggjkee.revolut.domain.ConversionRate
import javax.inject.Inject

class CurrencyConverter @Inject constructor() {

    fun convert(amount: Double, rate: ConversionRate): Double {
        return (amount * rate.rate)
    }
}