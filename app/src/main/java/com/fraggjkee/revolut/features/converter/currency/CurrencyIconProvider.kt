package com.fraggjkee.revolut.features.converter.currency

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.fraggjkee.revolut.R
import com.fraggjkee.revolut.domain.Currency
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrencyIconProvider @Inject constructor(private val context: Context) {

    private val mapping = mapOf(
        "EUR" to R.drawable.ic_european_union,
        "GBP" to R.drawable.ic_united_kingdom,
        "CNY" to R.drawable.ic_china,
        "USD" to R.drawable.ic_united_states,
        "CAD" to R.drawable.ic_canada,
        "RUB" to R.drawable.ic_russia

        // TODO ...request more icons from the UI/UX team :)
    )

    fun provide(currency: Currency): Drawable? {
        val currencyName = currency.code.toUpperCase(Locale.getDefault())
        return mapping[currencyName]?.let { ContextCompat.getDrawable(context, it) }
    }
}