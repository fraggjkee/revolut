package com.fraggjkee.revolut.features.base

interface Clearable {

    fun onCleared()
}