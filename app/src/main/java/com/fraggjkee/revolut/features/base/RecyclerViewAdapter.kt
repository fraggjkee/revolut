package com.fraggjkee.revolut.features.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fraggjkee.revolut.BR

class RecyclerViewAdapter : RecyclerView.Adapter<BindingViewHolder>() {

    private val items = mutableListOf<RecyclerViewItem>()

    init {
        setHasStableIds(true)
    }

    override fun getItemCount(): Int = items.size
    override fun getItemId(position: Int): Long = getItem(position).layoutId.toLong()
    override fun getItemViewType(position: Int): Int = getItem(position).layoutId

    override fun onCreateViewHolder(parent: ViewGroup, layoutId: Int): BindingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(inflater, layoutId, parent, false)
        return BindingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        getItem(position).bind(holder.binding)
        holder.binding.executePendingBindings()
    }

    fun updateData(newItems: List<RecyclerViewItem>) {
        val callback = DiffUtilCallback(
            oldList = this.items,
            newList = newItems
        )
        val diffResult = DiffUtil.calculateDiff(callback)

        this.items.clear()
        this.items.addAll(newItems)
        diffResult.dispatchUpdatesTo(this)
    }

    private fun getItem(position: Int): RecyclerViewItem = items[position]
}

private class DiffUtilCallback(
    private val oldList: List<RecyclerViewItem>,
    private val newList: List<RecyclerViewItem>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}

data class RecyclerViewItem(
    val viewModel: Any,
    val viewStateExtractor: (viewModel: Any) -> Any = { viewModel },
    @LayoutRes val layoutId: Int
) {
    private val viewState: Any = viewStateExtractor.invoke(viewModel)

    fun bind(binding: ViewDataBinding) {
        binding.setVariable(BR.viewState, viewState)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as RecyclerViewItem
        return (viewModel == other.viewModel)
    }

    override fun hashCode(): Int = viewModel.hashCode()
}

class BindingViewHolder(
    val binding: ViewDataBinding
) : RecyclerView.ViewHolder(binding.root)