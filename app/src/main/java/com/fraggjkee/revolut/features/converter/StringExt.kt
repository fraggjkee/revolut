package com.fraggjkee.revolut.features.converter

import java.text.NumberFormat
import java.text.ParseException
import java.util.*

// Regular String.toDouble() does not handle some locales
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun String.parseDouble(): Double {
    val numberFormat = NumberFormat.getInstance(Locale.getDefault())
    return try {
        numberFormat.parse(this).toDouble()
    } catch (e: ParseException) {
        0.0
    }
}