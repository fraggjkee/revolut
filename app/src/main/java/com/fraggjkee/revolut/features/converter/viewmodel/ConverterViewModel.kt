package com.fraggjkee.revolut.features.converter.viewmodel

import android.content.res.Resources
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.fraggjkee.revolut.R
import com.fraggjkee.revolut.data.repository.DEFAULT_CURRENCY
import com.fraggjkee.revolut.data.usecase.ObserveRatesUseCase
import com.fraggjkee.revolut.di.PerFragment
import com.fraggjkee.revolut.domain.ConversionRate
import com.fraggjkee.revolut.domain.Rates
import com.fraggjkee.revolut.features.base.Clearable
import com.fraggjkee.revolut.features.base.RecyclerViewItem
import com.fraggjkee.revolut.features.converter.BaseAmountStream
import com.fraggjkee.revolut.features.converter.parseDouble
import com.fraggjkee.revolut.features.converter.viewstate.ConverterViewState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import java.net.UnknownHostException
import java.util.Collections.swap
import javax.inject.Inject
import javax.inject.Provider

private const val DEFAULT_BASE_AMOUNT = 1.0

@PerFragment
class ConverterViewModel @Inject constructor(
    val viewState: ConverterViewState,
    private val observeRatesUseCase: ObserveRatesUseCase,
    private val itemVmProvider: Provider<CurrencyItemViewModel>,
    private val baseAmountStream: BaseAmountStream,
    private val resources: Resources
) : ViewModel(), LifecycleObserver {

    private var baseCurrency = DEFAULT_CURRENCY
    private val events = PublishSubject.create<Event>()
    private val disposables = CompositeDisposable()

    init {
        setupBaseAmountStream()
    }

    private fun setupBaseAmountStream() {
        baseAmountStream.post(DEFAULT_BASE_AMOUNT)
    }

    @Suppress("unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() = observeRates()

    private fun observeRates() {
        disposables += observeRatesUseCase.execute(base = baseCurrency)
            .doOnSubscribe { viewState.isLoading.set(true) }
            .doOnNext { viewState.isLoading.set(false) }
            .doOnError { viewState.isLoading.set(false) }
            .subscribeBy(
                onNext = { handleRatesChange(it) },
                onError = { handleError(it) }
            )
    }

    private fun handleRatesChange(rates: Rates) {
        val baseRate = ConversionRate(baseCurrency, baseCurrency, 1.0)
        val conversionRates = listOf(baseRate) + rates.data
        val recyclerItems = conversionRates.map { createCurrencyItemVm(it) }
            .filter { it.viewState.icon.get() != null } // FIXME: hide currencies I didn't add icons for
            .map { it.toRecyclerItem() }
        viewState.items.set(recyclerItems)
    }

    private fun createCurrencyItemVm(conversionRate: ConversionRate): CurrencyItemViewModel {
        return itemVmProvider.get()
            .also { it.setup(conversionRate) }
            .also { subscribeOnItemEvents(it) }
    }

    private fun subscribeOnItemEvents(viewModel: CurrencyItemViewModel) {
        disposables += viewModel.events()
            .observeOn(AndroidSchedulers.mainThread())
            .ofType<CurrencyItemViewModel.UiEvent.OnCellClick>()
            .subscribe { handleCellClick(it.sender) }
    }

    private fun handleCellClick(sender: CurrencyItemViewModel) {
        val isBaseCurrencyCellClicked = sender.conversionRate.isBaseCurrency
        if (isBaseCurrencyCellClicked.not()) {
            updateBaseCurrency(sender)
        }
    }

    private fun updateBaseCurrency(viewModel: CurrencyItemViewModel) {
        val items = viewState.items.get().orEmpty()
        val index = items.map { it.viewModel }
            .filterIsInstance<CurrencyItemViewModel>()
            .indexOf(viewModel)

        check(index != -1) { "handleCellClick: $viewModel not found in the list" }

        // Update view state
        val copy = items.toMutableList().also { swap(it, 0, index) }
        viewState.items.set(copy)

        // Reload data with the new base currency
        disposables.clear()
        baseCurrency = viewModel.conversionRate.target
        baseAmountStream.post(viewModel.viewState.amount.get()?.parseDouble() ?: 0.0)
        observeRates()
    }

    private fun handleError(throwable: Throwable) {
        val errorMessage =
            if (throwable is UnknownHostException) resources.getString(R.string.network_error)
            else resources.getString(R.string.rates_loading_error)
        events.onNext(
            Event.OnError(
                errorMessage
            )
        )
    }

    @Suppress("unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() = disposables.clear()

    fun events(): Observable<Event> = events.hide()

    override fun onCleared() {
        super.onCleared()

        disposables.dispose()

        viewState.items.get().orEmpty()
            .map { it.viewModel }
            .filterIsInstance<Clearable>()
            .forEach { it.onCleared() }
    }

    sealed class Event {
        class OnError(val message: String) : Event()
    }
}

private fun CurrencyItemViewModel.toRecyclerItem() = RecyclerViewItem(
    viewModel = this,
    viewStateExtractor = { this.viewState },
    layoutId = R.layout.item_converter_currency
)
