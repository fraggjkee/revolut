package com.fraggjkee.revolut.features.converter.di

import androidx.lifecycle.ViewModel
import com.fraggjkee.revolut.di.PerFragment
import com.fraggjkee.revolut.features.base.viewmodel.ViewModelKey
import com.fraggjkee.revolut.features.converter.ConverterFragment
import com.fraggjkee.revolut.features.converter.viewmodel.ConverterViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface RatesFragmentModule {

    @ContributesAndroidInjector(modules = [RatesFragmentViewModelModule::class])
    @PerFragment
    fun contributeFragment(): ConverterFragment
}

@Module
private interface RatesFragmentViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ConverterViewModel::class)
    @PerFragment
    fun bindViewModel(viewModel: ConverterViewModel): ViewModel
}