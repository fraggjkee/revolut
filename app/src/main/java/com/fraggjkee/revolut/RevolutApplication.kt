package com.fraggjkee.revolut

import android.app.Application
import com.fraggjkee.revolut.di.AppComponent
import com.fraggjkee.revolut.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class RevolutApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Any>

    private lateinit var appComponent: AppComponent

    override fun androidInjector(): AndroidInjector<Any> = activityInjector

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
            .also { it.inject(this) }
    }
}