package com.fraggjkee.revolut.domain

object CurrencyFactory {

    fun withCode(currencyCode: String): Currency {
        return currencyCode.toCurrency()
    }

    private fun String.toCurrency() = Currency(
        code = this,
        displayName = getCurrencyDisplayName(this)
    )

    private fun getCurrencyDisplayName(currencyCode: String): String {
        return try {
            java.util.Currency.getInstance(currencyCode).displayName
        } catch (e: IllegalArgumentException) {
            currencyCode
        }
    }
}