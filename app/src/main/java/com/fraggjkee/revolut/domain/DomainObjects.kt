package com.fraggjkee.revolut.domain

data class Currency(
    val code: String,
    val displayName: String
)

data class ConversionRate(
    val target: Currency,
    val base: Currency,
    val rate: Double
)

data class Rates(
    val base: Currency,
    val data: List<ConversionRate>
)