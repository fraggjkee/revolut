package com.fraggjkee.revolut.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.fraggjkee.revolut.MainActivity
import com.fraggjkee.revolut.data.di.DataModule
import com.fraggjkee.revolut.features.base.viewmodel.ViewModelFactoryModule
import com.fraggjkee.revolut.features.converter.di.RatesFragmentModule
import com.fraggjkee.revolut.network.di.NetworkModule
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module(
    includes = [
        NetworkModule::class,
        ContextModule::class,
        DataModule::class,
        ViewModelFactoryModule::class
    ]
)
interface AppModule {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    @PerActivity
    fun contributeMainActivity(): MainActivity
}

@Module
private object ContextModule {

    @Singleton
    @Provides
    fun provideAppContext(application: Application): Context = application.applicationContext

    @Singleton
    @Provides
    fun provideResources(application: Application): Resources = application.resources
}

@Module(includes = [RatesFragmentModule::class])
private interface ActivityModule
