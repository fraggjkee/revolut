package com.fraggjkee.revolut.data.usecase

import com.fraggjkee.revolut.data.repository.DEFAULT_CURRENCY
import com.fraggjkee.revolut.domain.Currency
import com.fraggjkee.revolut.domain.Rates
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

private val REFRESH_RATE_DEFAULT = TimeUnit.SECONDS.toMillis(3)

interface ObserveRatesUseCase {

    fun execute(
        base: Currency = DEFAULT_CURRENCY,
        refreshRateMs: Long = REFRESH_RATE_DEFAULT
    ): Observable<Rates>
}