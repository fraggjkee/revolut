package com.fraggjkee.revolut.data.repository

import com.fraggjkee.revolut.data.toDomain
import com.fraggjkee.revolut.domain.Currency
import com.fraggjkee.revolut.domain.Rates
import com.fraggjkee.revolut.network.RatesApi
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RatesRepositoryImpl @Inject constructor(
    private val ratesApi: RatesApi
) : RatesRepository {

    override fun getRates(base: Currency): Single<Rates> {
        return ratesApi.getRates(base.code)
            .map { it.toDomain() }
            .subscribeOn(Schedulers.io())
    }
}