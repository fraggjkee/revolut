package com.fraggjkee.revolut.data.di

import com.fraggjkee.revolut.data.repository.RatesRepository
import com.fraggjkee.revolut.data.repository.RatesRepositoryImpl
import com.fraggjkee.revolut.data.usecase.ObserveRatesUseCase
import com.fraggjkee.revolut.data.usecase.ObserveRatesUseCaseImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module(
    includes = [
        RatesRepositoryModule::class,
        RatesUseCasesModule::class
    ]
)
interface DataModule

@Module
private interface RatesRepositoryModule {

    @Singleton
    @Binds
    fun provideRatesRepository(repositoryImpl: RatesRepositoryImpl): RatesRepository
}

@Module
private interface RatesUseCasesModule {

    @Binds
    fun provideObserveRatesUseCase(useCaseImpl: ObserveRatesUseCaseImpl): ObserveRatesUseCase
}