package com.fraggjkee.revolut.data

import com.fraggjkee.revolut.domain.ConversionRate
import com.fraggjkee.revolut.domain.Currency
import com.fraggjkee.revolut.domain.CurrencyFactory
import com.fraggjkee.revolut.domain.Rates
import com.fraggjkee.revolut.network.GetRatesResponse

fun GetRatesResponse.toDomain() = Rates(
    base = this.base.toCurrency(),
    data = this.rates.toRates(this.base)
)

private fun String.toCurrency(): Currency {
    return CurrencyFactory.withCode(this)
}

private fun Map<String, Double>.toRates(base: String): List<ConversionRate> {
    return this.entries.map { (key, value) ->
        ConversionRate(
            target = key.toCurrency(),
            base = base.toCurrency(),
            rate = value
        )
    }
}