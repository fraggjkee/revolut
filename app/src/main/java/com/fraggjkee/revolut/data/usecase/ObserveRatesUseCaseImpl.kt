package com.fraggjkee.revolut.data.usecase

import com.fraggjkee.revolut.data.repository.RatesRepository
import com.fraggjkee.revolut.domain.Currency
import com.fraggjkee.revolut.domain.Rates
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ObserveRatesUseCaseImpl @Inject constructor(
    private val repository: RatesRepository
) : ObserveRatesUseCase {

    override fun execute(base: Currency, refreshRateMs: Long): Observable<Rates> {
        return Observable
            .interval(0, refreshRateMs, TimeUnit.MILLISECONDS)
            .switchMap { repository.getRates(base).toObservable() }
            .observeOn(Schedulers.io())
    }
}