package com.fraggjkee.revolut.data.repository

import com.fraggjkee.revolut.domain.Currency
import com.fraggjkee.revolut.domain.CurrencyFactory
import com.fraggjkee.revolut.domain.Rates
import io.reactivex.Single

val DEFAULT_CURRENCY = CurrencyFactory.withCode("EUR")

interface RatesRepository {

    fun getRates(base: Currency = DEFAULT_CURRENCY): Single<Rates>
}