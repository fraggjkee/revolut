package com.fraggjkee.revolut.features.converter.viewmodel

import android.content.res.Resources
import com.fraggjkee.revolut.RxAndroidSchedulerRule
import com.fraggjkee.revolut.data.repository.DEFAULT_CURRENCY
import com.fraggjkee.revolut.data.toDomain
import com.fraggjkee.revolut.data.usecase.ObserveRatesUseCase
import com.fraggjkee.revolut.data.usecase.ObserveRatesUseCaseImpl
import com.fraggjkee.revolut.domain.ConversionRate
import com.fraggjkee.revolut.domain.Currency
import com.fraggjkee.revolut.domain.Rates
import com.fraggjkee.revolut.features.converter.BaseAmountStream
import com.fraggjkee.revolut.features.converter.viewstate.ConverterViewState
import com.fraggjkee.revolut.mockGetRatesResponse
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyOrder
import io.reactivex.Observable
import org.junit.Rule
import org.junit.Test
import javax.inject.Provider

class ConverterViewModelTest {

    @get:Rule
    val rxSchedulerRule = RxAndroidSchedulerRule()

    @Test
    fun `GIVEN view model is in init state WHEN onStart() callback is received THEN view model begins rates observing`() {
        // GIVEN
        val useCase = mockk<ObserveRatesUseCase>(relaxed = true)
        val viewModel = mockViewModel(useCase = useCase)

        // WHEN
        viewModel.onStart()

        // THEN
        verify { useCase.execute(any(), any()) }
    }

    @Test
    fun `GIVEN use case returns a result WHEN onStart() is invoked THEN viewState's progress is updated`() {
        // GIVEN
        val viewState = mockk<ConverterViewState>(relaxed = true)
        val useCase = mockk<ObserveRatesUseCase>()
        val rates = mockGetRatesResponse().toDomain()
        every { useCase.execute(any(), any()) } returns Observable.just(rates)

        val viewModel = mockViewModel(
            viewState = viewState,
            useCase = useCase
        )

        // WHEN
        viewModel.onStart()

        // THEN
        verifyOrder {
            viewState.isLoading.set(eq(true))
            viewState.isLoading.set(eq(false))
        }
    }

    @Test
    fun `GIVEN use case returns Rates WHEN onStart() is invoked THEN viewState's items are updated`() {
        // GIVEN
        val viewState = ConverterViewState()
        val useCase = mockk<ObserveRatesUseCase>()
        val rates = mockGetRatesResponse().toDomain()
        every { useCase.execute(any(), any()) } returns Observable.just(rates)

        val itemProvider = mockk<Provider<CurrencyItemViewModel>>()
        every { itemProvider.get() } returns mockk(relaxed = true)

        val viewModel = mockViewModel(
            viewState = viewState,
            useCase = useCase,
            itemVmProvider = itemProvider
        )

        // WHEN
        viewModel.onStart()

        // THEN
        val recyclerItems = viewState.items.get()
        val expectedSize = rates.data.size + 1 // items from the response + 1 base element
        assertThat(recyclerItems).hasSize(expectedSize)
    }

    @Test
    fun `GIVEN use case returns Rates WHEN onStart() is invoked THEN child view models are created and initialized`() {
        // GIVEN
        val viewState = ConverterViewState()

        val useCase = mockk<ObserveRatesUseCaseImpl>()
        val defaultCurrency = DEFAULT_CURRENCY
        val rates = Rates(
            base = defaultCurrency,
            data = listOf(
                ConversionRate(
                    Currency("USD", "Dollar"),
                    base = defaultCurrency,
                    rate = 1.0
                )
            )
        )
        every { useCase.execute(any()) } returns Observable.just(rates)

        val itemViewModel = mockk<CurrencyItemViewModel>(relaxed = true)
        val itemProvider = mockk<Provider<CurrencyItemViewModel>>()
        every { itemProvider.get() } returns itemViewModel

        val viewModel = mockViewModel(
            viewState = viewState,
            useCase = useCase,
            itemVmProvider = itemProvider
        )

        // WHEN
        viewModel.onStart()

        // THEN
        val expectedAmount = rates.data.size.inc()
        verify(exactly = expectedAmount) { itemProvider.get() }
        verify(exactly = expectedAmount) { itemViewModel.events() }
        verifyOrder {
            itemViewModel.setup(ConversionRate(defaultCurrency, defaultCurrency, 1.0))
            itemViewModel.setup(rates.data.first())
        }
    }

    @Test
    fun `GIVEN use case returns error WHEN onStart() is invoked THEN error event is sent`() {
        // GIVEN
        val useCase = mockk<ObserveRatesUseCaseImpl>()
        every { useCase.execute(any()) } returns Observable.error(Exception())

        val viewModel = mockViewModel(
            useCase = useCase
        )

        // WHEN
        val testSubscriber = viewModel.events().test()
        viewModel.onStart()

        // THEN
        testSubscriber.assertNotComplete()
            .assertValueCount(1)
            .assertValue { it is ConverterViewModel.Event.OnError }
    }
}

private fun mockViewModel(
    viewState: ConverterViewState = mockk(relaxed = true),
    useCase: ObserveRatesUseCase = mockk(relaxed = true),
    itemVmProvider: Provider<CurrencyItemViewModel> = mockk(relaxed = true),
    amountStream: BaseAmountStream = mockk(relaxed = true),
    resources: Resources = mockk(relaxed = true)
) = ConverterViewModel(
    viewState = viewState,
    observeRatesUseCase = useCase,
    itemVmProvider = itemVmProvider,
    baseAmountStream = amountStream,
    resources = resources
)