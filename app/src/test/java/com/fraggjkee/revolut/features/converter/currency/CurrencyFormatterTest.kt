package com.fraggjkee.revolut.features.converter.currency

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class CurrencyFormatterTest {

    private val formatter = CurrencyFormatter()

    @Test
    fun `GIVEN non-fractional amount WHEN format() is invoked THEN integer string is returned`() {
        // GIVEN
        val amount = 1.0

        // WHEN
        val result = formatter.format(amount)

        // THEN
        assertThat(result).isEqualTo("1")
    }

    @Test
    fun `GIVEN fractional number WHEN format() is invoked THEN properly formatted string is returned`() {
        // GIVEN
        val amount = 1.011111

        // WHEN
        val result = formatter.format(amount)

        // THEN
        assertThat(result).isEqualTo("1.01")
    }

    @Test
    fun `GIVEN fractional number WHEN format() is invoked THEN rounded formatted string is returned`() {
        // GIVEN
        val amount = 1.019

        // WHEN
        val result = formatter.format(amount)

        // THEN
        assertThat(result).isEqualTo("1.02")
    }

    // ...and so on (test different locales, etc.)
}