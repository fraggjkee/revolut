package com.fraggjkee.revolut.data.repository

import com.fraggjkee.revolut.RxAndroidSchedulerRule
import com.fraggjkee.revolut.data.toDomain
import com.fraggjkee.revolut.domain.Currency
import com.fraggjkee.revolut.mockGetRatesResponse
import com.fraggjkee.revolut.network.RatesApi
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Rule
import org.junit.Test

class RatesRepositoryTest {

    @get:Rule
    val rxSchedulerRule = RxAndroidSchedulerRule()

    @Test
    fun `GIVEN repository is instantiated with api WHEN getRates() is executed THEN appropriate api is also executed`() {
        // GIVEN
        val currency = Currency(code = "EUR", displayName = "Euro")
        val ratesApi = mockk<RatesApi>(relaxed = true)
        val repository = RatesRepositoryImpl(ratesApi)

        // WHEN
        repository.getRates(currency).test()

        // THEN
        verify { ratesApi.getRates(base = currency.code) }
    }

    @Test
    fun `GIVEN repository is instantiated with api WHEN getRates() with no params is executed THEN default currency is used`() {
        // GIVEN
        val defaultCurrency = DEFAULT_CURRENCY
        val ratesApi = mockk<RatesApi>(relaxed = true)
        val repository = RatesRepositoryImpl(ratesApi)

        // WHEN
        repository.getRates().test()

        // THEN
        verify { ratesApi.getRates(base = defaultCurrency.code) }
    }

    @Test
    fun `GIVEN api returns valid response WHEN getRates() is executed THEN repository returns mapped domain object`() {
        // GIVEN
        val ratesApi = mockk<RatesApi>(relaxed = true)
        val mockResponse = mockGetRatesResponse()
        every { ratesApi.getRates(any()) } returns Single.just(mockResponse)
        val repository = RatesRepositoryImpl(ratesApi)

        // WHEN
        val testObserver = repository.getRates().test()

        // THEN
        testObserver.assertValue { it == mockResponse.toDomain() }
    }
}