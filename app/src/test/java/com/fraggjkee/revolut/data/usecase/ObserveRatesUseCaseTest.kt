package com.fraggjkee.revolut.data.usecase

import com.fraggjkee.revolut.data.repository.DEFAULT_CURRENCY
import com.fraggjkee.revolut.data.repository.RatesRepositoryImpl
import com.fraggjkee.revolut.domain.Rates
import com.fraggjkee.revolut.network.RatesApi
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Test

class ObserveRatesUseCaseTest {

    @Test
    fun `GIVEN use case is instantiated with a repository WHEN execute() is invoked THEN repository is fetched`() {
        // GIVEN
        val currency = DEFAULT_CURRENCY
        val ratesApi = mockk<RatesApi>(relaxed = true)
        val repository = spyk(
            RatesRepositoryImpl(ratesApi)
        )
        val useCase = ObserveRatesUseCaseImpl(repository)

        // WHEN
        useCase.execute(currency).test()

        // THEN
        verify { repository.getRates(base = currency) }
    }

    @Test
    fun `GIVEN observing is in progress WHEN execute() is invoked THEN multiple requests are executed`() {
        // GIVEN
        val refreshRate = 10L
        val mockResponse = Rates(DEFAULT_CURRENCY, emptyList())
        val responseList = listOf(
            Single.just(mockResponse),
            Single.just(mockResponse),
            Single.just(mockResponse),
            Single.error(Exception()) // return error to terminate the stream
        )
        val repository = mockk<RatesRepositoryImpl>(relaxed = true)
        every { repository.getRates(any()) } returnsMany responseList
        val useCase = ObserveRatesUseCaseImpl(repository)

        // WHEN
        val testObserver = useCase.execute(
            base = DEFAULT_CURRENCY,
            refreshRateMs = refreshRate
        ).test()

        // THEN
        testObserver.await().assertValueCount(responseList.size.dec())
        verify(exactly = responseList.size) { repository.getRates(any()) }
    }
}