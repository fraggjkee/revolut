package com.fraggjkee.revolut

import com.fraggjkee.revolut.network.GetRatesResponse
import com.squareup.moshi.Moshi

fun mockGetRatesResponse() = Moshi.Builder()
    .build()
    .adapter(GetRatesResponse::class.java)
    .fromJson(GET_RATES_RESPONSE_JSON)!!

const val GET_RATES_RESPONSE_JSON =
    """
    {
    "base": "EUR",
    "date": "2018-09-06",
    "rates": {
        "AUD": 1.6167,
        "BGN": 1.9561,
        "BRL": 4.7926,
        "CAD": 1.534,
        "CHF": 1.1277,
        "CNY": 7.9464,
        "CZK": 25.719,
        "DKK": 7.4579,
        "GBP": 0.89838,
        "HKD": 9.1339,
        "HRK": 7.4353,
        "HUF": 326.54,
        "IDR": 17326.0,
        "ILS": 4.1713,
        "INR": 83.731,
        "ISK": 127.82,
        "JPY": 129.57,
        "KRW": 1305.0,
        "MXN": 22.369,
        "MYR": 4.8128,
        "NOK": 9.7776,
        "NZD": 1.7636,
        "PHP": 62.602,
        "PLN": 4.319,
        "RON": 4.6392,
        "RUB": 79.587,
        "SEK": 10.593,
        "SGD": 1.6003,
        "THB": 38.136,
        "TRY": 7.6294,
        "USD": 1.1636,
        "ZAR": 17.826
        }
    } 
    """